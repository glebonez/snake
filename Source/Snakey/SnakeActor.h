// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeActor.generated.h"

class ASnakeElementBase;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};
UCLASS()
class SNAKEY_API ASnakeActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeActor();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementBase> SnakeElementClass;
	UPROPERTY()
		TArray <ASnakeElementBase*> SnakeElements;
	UPROPERTY(EditDefaultsOnly)
		float ElementSize;
	UPROPERTY()
		EMovementDirection LastMovementDirection;
	UPROPERTY(EditDefaultsOnly)
		float MovementSpeed;

protected:
	
	virtual void BeginPlay() override;

public:	
	 
	virtual void Tick(float DeltaTime) override;

	void AddSnakeElement(int ElementsNum = 1);

	void Move();

};
