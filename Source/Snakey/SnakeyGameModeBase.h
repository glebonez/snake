// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeyGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEY_API ASnakeyGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
