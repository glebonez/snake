// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEY_SnakeActor_generated_h
#error "SnakeActor.generated.h already included, missing '#pragma once' in SnakeActor.h"
#endif
#define SNAKEY_SnakeActor_generated_h

#define Snakey_Source_Snakey_SnakeActor_h_22_SPARSE_DATA
#define Snakey_Source_Snakey_SnakeActor_h_22_RPC_WRAPPERS
#define Snakey_Source_Snakey_SnakeActor_h_22_RPC_WRAPPERS_NO_PURE_DECLS
#define Snakey_Source_Snakey_SnakeActor_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakeActor(); \
	friend struct Z_Construct_UClass_ASnakeActor_Statics; \
public: \
	DECLARE_CLASS(ASnakeActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snakey"), NO_API) \
	DECLARE_SERIALIZER(ASnakeActor)


#define Snakey_Source_Snakey_SnakeActor_h_22_INCLASS \
private: \
	static void StaticRegisterNativesASnakeActor(); \
	friend struct Z_Construct_UClass_ASnakeActor_Statics; \
public: \
	DECLARE_CLASS(ASnakeActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snakey"), NO_API) \
	DECLARE_SERIALIZER(ASnakeActor)


#define Snakey_Source_Snakey_SnakeActor_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeActor(ASnakeActor&&); \
	NO_API ASnakeActor(const ASnakeActor&); \
public:


#define Snakey_Source_Snakey_SnakeActor_h_22_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeActor(ASnakeActor&&); \
	NO_API ASnakeActor(const ASnakeActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASnakeActor)


#define Snakey_Source_Snakey_SnakeActor_h_22_PRIVATE_PROPERTY_OFFSET
#define Snakey_Source_Snakey_SnakeActor_h_19_PROLOG
#define Snakey_Source_Snakey_SnakeActor_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snakey_Source_Snakey_SnakeActor_h_22_PRIVATE_PROPERTY_OFFSET \
	Snakey_Source_Snakey_SnakeActor_h_22_SPARSE_DATA \
	Snakey_Source_Snakey_SnakeActor_h_22_RPC_WRAPPERS \
	Snakey_Source_Snakey_SnakeActor_h_22_INCLASS \
	Snakey_Source_Snakey_SnakeActor_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snakey_Source_Snakey_SnakeActor_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snakey_Source_Snakey_SnakeActor_h_22_PRIVATE_PROPERTY_OFFSET \
	Snakey_Source_Snakey_SnakeActor_h_22_SPARSE_DATA \
	Snakey_Source_Snakey_SnakeActor_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	Snakey_Source_Snakey_SnakeActor_h_22_INCLASS_NO_PURE_DECLS \
	Snakey_Source_Snakey_SnakeActor_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEY_API UClass* StaticClass<class ASnakeActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Snakey_Source_Snakey_SnakeActor_h


#define FOREACH_ENUM_EMOVEMENTDIRECTION(op) \
	op(EMovementDirection::UP) \
	op(EMovementDirection::DOWN) \
	op(EMovementDirection::LEFT) \
	op(EMovementDirection::RIGHT) 

enum class EMovementDirection;
template<> SNAKEY_API UEnum* StaticEnum<EMovementDirection>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
