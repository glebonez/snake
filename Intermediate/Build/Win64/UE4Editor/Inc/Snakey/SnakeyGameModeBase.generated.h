// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEY_SnakeyGameModeBase_generated_h
#error "SnakeyGameModeBase.generated.h already included, missing '#pragma once' in SnakeyGameModeBase.h"
#endif
#define SNAKEY_SnakeyGameModeBase_generated_h

#define Snakey_Source_Snakey_SnakeyGameModeBase_h_15_SPARSE_DATA
#define Snakey_Source_Snakey_SnakeyGameModeBase_h_15_RPC_WRAPPERS
#define Snakey_Source_Snakey_SnakeyGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Snakey_Source_Snakey_SnakeyGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakeyGameModeBase(); \
	friend struct Z_Construct_UClass_ASnakeyGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeyGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snakey"), NO_API) \
	DECLARE_SERIALIZER(ASnakeyGameModeBase)


#define Snakey_Source_Snakey_SnakeyGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASnakeyGameModeBase(); \
	friend struct Z_Construct_UClass_ASnakeyGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeyGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snakey"), NO_API) \
	DECLARE_SERIALIZER(ASnakeyGameModeBase)


#define Snakey_Source_Snakey_SnakeyGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeyGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeyGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeyGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeyGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeyGameModeBase(ASnakeyGameModeBase&&); \
	NO_API ASnakeyGameModeBase(const ASnakeyGameModeBase&); \
public:


#define Snakey_Source_Snakey_SnakeyGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeyGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeyGameModeBase(ASnakeyGameModeBase&&); \
	NO_API ASnakeyGameModeBase(const ASnakeyGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeyGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeyGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeyGameModeBase)


#define Snakey_Source_Snakey_SnakeyGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define Snakey_Source_Snakey_SnakeyGameModeBase_h_12_PROLOG
#define Snakey_Source_Snakey_SnakeyGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snakey_Source_Snakey_SnakeyGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Snakey_Source_Snakey_SnakeyGameModeBase_h_15_SPARSE_DATA \
	Snakey_Source_Snakey_SnakeyGameModeBase_h_15_RPC_WRAPPERS \
	Snakey_Source_Snakey_SnakeyGameModeBase_h_15_INCLASS \
	Snakey_Source_Snakey_SnakeyGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snakey_Source_Snakey_SnakeyGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snakey_Source_Snakey_SnakeyGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Snakey_Source_Snakey_SnakeyGameModeBase_h_15_SPARSE_DATA \
	Snakey_Source_Snakey_SnakeyGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Snakey_Source_Snakey_SnakeyGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	Snakey_Source_Snakey_SnakeyGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEY_API UClass* StaticClass<class ASnakeyGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Snakey_Source_Snakey_SnakeyGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
